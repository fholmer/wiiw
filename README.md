# WIIW

Build a Windows installer in Wine!

* Source code: https://gitlab.com/fholmer/wiiw
* Docker hub: https://hub.docker.com/r/fholmer/wiiw

## Overview

A set of tools to build and package a Windows installer using Inno-Setup in Wine.

Pre-installed tools:

- iscc (inno setup)
- pip
- pyinstaller
- python

## Getting started

Assume you have the following source code files:

```
/my-source-code-dir/
    /requirements.txt
    /setup.py
    /your_program.py
    /your_setup.iss
```

Run the container

```
cd my-source-code-dir
docker run -it --rm -v $PWD:/work fholmer/wiiw /bin/bash
```

Get dependencies:

```
wine pip -r requirements.txt
```


Build a pip package:

```
wine python setup.py bdist_wheel
```

Bundle your application and all its dependencies:

```
wine pyinstaller your_program.py
```

Create a inno setup file:

```
wine ISCC.exe your_setup.iss
```

## Links

- Inno setup ([jrsoftware.org](https://jrsoftware.org/))
- Inno setup NuGet package [Tools.InnoSetup](https://www.nuget.org/packages/Tools.InnoSetup)
- pip [pip.pypa.io](https://pip.pypa.io/)
- pyinstaller ([pyinstaller.org](https://pyinstaller.org/))
- Python [python.org](https://www.python.org/)
- Python NuGet package [Python](https://www.nuget.org/packages/python/)

#!/usr/bin/env sh

action=$1
package=$2
version=$3

if test "$action" = "install"
  then
    echo Fetching package $package==$version from nuget.org
    curl -SLf -o "$package.$version.zip" "https://www.nuget.org/api/v2/package/$package/$version"
    unzip "$package.$version.zip" "tools/*" -d "$package.$version"
    ls "$package.$version"/tools/*.exe
else
    echo Unknown action: $action
fi

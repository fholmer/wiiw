FROM debian:bullseye

RUN set -x \
    && apt-get update -qy \
    && apt-get install --no-install-recommends -qy wine curl unzip ca-certificates \
    && dpkg --add-architecture i386 \
    && apt-get update -qy \
    && apt-get install --no-install-recommends -qy wine wine32

WORKDIR /build

COPY from-nuget.sh .
RUN chmod +x from-nuget.sh

ARG PYTHON_VERSION=3.10.6
ARG INNO_VERSION=6.2.1

ENV WINEDEBUG fixme-all
ENV WINEPATH Z:\\build\\python.$PYTHON_VERSION\\tools;\
Z:\\build\\python.$PYTHON_VERSION\\tools\\Scripts;\
Z:\\build\\Tools.InnoSetup.$INNO_VERSION\\tools

RUN set -x \
    && ./from-nuget.sh install python $PYTHON_VERSION \
    && ./from-nuget.sh install Tools.InnoSetup $INNO_VERSION

RUN wine64 "python.$PYTHON_VERSION/tools/python.exe" \
    -m pip install -U pip twine wheel pyinstaller

WORKDIR /work
VOLUME /work
